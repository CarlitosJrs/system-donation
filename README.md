This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Sistema de Donación para Ayuda a Perros y Gatos Callejeros

Este proyecto es un sistema de donación diseñado para ayudar a perros y gatos callejeros. Los usuarios pueden realizar donaciones a través de Mercado Pago para apoyar esta noble causa. La aplicación está construida utilizando Next.js, React.js, MongoDB y TailwindCSS.

## Características

- **Donaciones Seguras**: Integra la API de Mercado Pago para procesar donaciones de manera segura.
- **Registro de Donantes**: Guarda la información de los donantes en una base de datos MongoDB.
- **Página Informativa**: Proporciona información sobre cómo se utilizan las donaciones para ayudar a los animales callejeros.
- **Responsivo**: Diseñado para funcionar bien en dispositivos móviles y de escritorio.

## Tecnologías Utilizadas

- **Frontend**: React.js con Next.js
- **Backend**: Node.js
- **Base de Datos**: MongoDB
- **Procesamiento de Pagos**: Mercado Pago
- **Estilos**: TailwindCSS
