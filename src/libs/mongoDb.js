import mongoose from "mongoose";

//Variable de entorno de nuestro archivo '.env.local'
const { MONGODB_URL } = process.env;

if (!MONGODB_URL) {
  throw new Error("MONGODB_URI must be defined");
}

//Conectamos a la base de datos
export const connectDB = async () => {
  try {
    const { connection } = await mongoose.connect(MONGODB_URL);

    //Si hay conexion exitosa con la base de datos enviamos un mensaje
    if (connection.readyState === 1) {
      console.log("MongoDB connected");
      return Promise.resolve(true);
    }
  } catch (error) {
    console.log(error);
    return Promise.reject(false);
  }
};
