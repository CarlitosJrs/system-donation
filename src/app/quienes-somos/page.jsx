import Link from "next/link";
import React from "react";

const AboutPage = () => {
  return (
    <section id="quienes-somos" className="py-16 px-8 bg-white">
      <section className="overflow-hidden bg-[url(/perros-gatos-rescatados.avif)] bg-cover mb-8">
        <div className="bg-black/25 p-8 md:p-12 lg:px-16 lg:py-24">
          <div className="text-center sm:text-start ltr:sm:text-left rtl:sm:text-right">
            <h2 className="text-2xl font-bold text-white sm:text-3xl md:text-5xl">
              Ellos se merecen lo mejor
            </h2>

            <p className="text-sm md:text-base max-w-lg text-white/90 md:mt-6 md:block  md:leading-relaxed">
              En Huellitas Desamparadas, creemos que cada perro y gato callejero
              merece una vida llena de amor, cuidado y respeto. Trabajamos
              incansablemente para brindarles refugio, atención médica y la
              oportunidad de encontrar un hogar definitivo. Tu apoyo es esencial
              para hacer posible nuestro trabajo y transformar sus vidas para
              mejor. Únete a nosotros y juntos hagamos la diferencia.
            </p>
          </div>
        </div>
      </section>
      <div className="container mx-auto text-center ">
        <div className="bg-blue-50 p-8 rounded mb-8">
          <h2 className="text-4xl font-medium mb-8 text-slate-500">
            Nuestra Misión
          </h2>
          <p className="mb-8 text-sm md:text-lg leading-relaxed text-gray-500">
            Somos <strong>Huellitas Desamparadas</strong>, una organización sin
            fines de lucro dedicada a mejorar la calidad de vida de perros y
            gatos callejeros. Nuestra misión es brindarles refugio, atención
            médica, alimentación y, sobre todo, amor.
          </p>
        </div>

        <div className="mb-8 bg-blue-50 p-4 rounded">
          <h3 className="text-2xl font-medium mb-4 text-slate-500">
            Nuestro Compromiso
          </h3>
          <p className="text-sm md:text-lg leading-relaxed text-gray-500">
            Desde nuestros inicios, hemos trabajado incansablemente para
            rescatar animales en situación de abandono, proporcionándoles un
            hogar seguro y la atención que merecen. Creemos firmemente en el
            derecho de todos los animales a una vida digna y llena de cariño.
          </p>
        </div>

        <div className="mb-8 bg-blue-50 p-8 rounded">
          <h3 className="text-2xl font-semibold mb-4 text-slate-500">
            Cómo lo Hacemos
          </h3>
          <p className="text-sm md:text-lg leading-relaxed text-gray-500">
            Gracias a las generosas donaciones de personas como tú, podemos:
          </p>
          <ul className="list-disc list-inside text-sm md:text-lg leading-relaxed text-gray-500">
            <li>
              <strong>Rescatar:</strong> Localizamos y rescatamos animales en
              riesgo, proporcionándoles un ambiente seguro.
            </li>
            <li>
              <strong>Cuidar:</strong> Les brindamos atención médica,
              vacunaciones y tratamientos necesarios.
            </li>
            <li>
              <strong>Alimentar:</strong> Aseguramos que cada animal tenga una
              nutrición adecuada para su recuperación y bienestar.
            </li>
            <li>
              <strong>Rehabilitar:</strong> Trabajamos en la socialización y
              rehabilitación para que puedan ser adoptados por familias
              amorosas.
            </li>
          </ul>
        </div>

        <div className="mb-8 bg-blue-50 p-8 rounded">
          <h3 className="text-2xl font-semibold mb-4 text-slate-500">
            Únete a Nosotros
          </h3>
          <p className="text-sm md:text-lg leading-relaxed text-gray-500">
            En <strong>Huellitas Desamparadas</strong>, cada donación cuenta. Ya
            sea a través de aportes monetarios, donación de alimentos, medicinas
            o tu tiempo como voluntario, tu ayuda es invaluable. Juntos, podemos
            hacer una diferencia en la vida de estos animales.
          </p>
        </div>

        <div className="flex justify-center gap-4">
          <Link
            href="/donar"
            className="bg-green-500 text-white py-2 px-4 rounded"
          >
            Haz una Donación
          </Link>
        </div>
      </div>
    </section>
  );
};

export default AboutPage;
