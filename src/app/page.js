import HeroSection from "@/components/HeroSection";
import Image from "next/image";

export default function Home() {
  return (
    <>
      <HeroSection />
      <section className="p-8 flex flex-col md:flex-row">
        <div className="w-full md:w-[40%] p-8 bg-gray-50 flex justify-center items-center">
          <div>
            <h2 className="text-xl md:text-3xl text-center font-bold text-blue-500">
              Ellos te necesitan
              <span className="ml-2">
                <Image
                  src={"/corazon-edit.png"}
                  width={50}
                  height={50}
                  alt="img"
                  className="inline-block"
                />
              </span>
            </h2>
          </div>
        </div>
        <div className="grow grid md:grid-cols-2 gap-4 mx-auto">
          <div>
            <Image
              src={"/gatos.webp"}
              width={300}
              height={300}
              alt="image"
              className="w-full h-full rounded"
            />
          </div>
          <div>
            <Image
              src={"/perro&gato.jpg"}
              width={300}
              height={300}
              alt="image"
              className="w-full h-full rounded"
            />
          </div>
          <div>
            <Image
              src={"/perros-abandonados-001.jpg"}
              width={300}
              height={300}
              alt="image"
              className="w-full h-full rounded"
            />
          </div>
          <div>
            <Image
              src={"/perros.jpg"}
              width={300}
              height={300}
              alt="image"
              className="w-full rounded"
            />
          </div>
        </div>
      </section>
    </>
  );
}
