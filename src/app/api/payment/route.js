import { connectDB } from "@/libs/mongoDb";
import Donation from "@/models/donation";
import { MercadoPagoConfig, Payment } from "mercadopago";
import { NextResponse } from "next/server";

const client = new MercadoPagoConfig({
  //Token de un usuario de prueba
  accessToken: process.env.MP_ACCESS_TOKEN_TEST,
});

export async function POST(req) {
  await connectDB();
  const body = await req.json();

  if (body.type === "payment") {
    const payment = await new Payment(client).get({ id: body.data.id });
    /*
      Aqui hacer validaciones para casos de fallos de la operacion de pago
    */

    //Doc para la base de datos
    const donation = {
      id: payment.id,
      amount: payment.transaction_amount,
      firstName: payment.additional_info.payer.first_name,
      lastName: payment.additional_info.payer.last_name,
      email: payment.payer.email,
    };

    //Creamos el documento en la base de datos
    const donationDoc = await Donation.create(donation);
    return NextResponse.json({ donationDoc });
  }
  return NextResponse.json(body);
}

export async function GET() {
  await connectDB();

  try {
    const donationsDoc = await Donation.find();

    if (!donationsDoc) {
      throw new Error("Hubo un error al obtener las donaciones");
    }

    return NextResponse.json(donationsDoc);
  } catch (error) {
    return NextResponse.json(error || error.message);
  }
}
