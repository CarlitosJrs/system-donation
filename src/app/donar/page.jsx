import { cards } from "@/helpers/help";
import Image from "next/image";
import React from "react";
import { MercadoPagoConfig, Preference } from "mercadopago";
import { redirect } from "next/navigation";

const client = new MercadoPagoConfig({
  //Token de un usuario de prueba
  accessToken: process.env.MP_ACCESS_TOKEN_TEST,
});

const DonationPage = () => {
  async function handleSubmit(formData) {
    "use server";

    const preference = await new Preference(client).create({
      body: {
        items: [
          {
            title: "Donacion",
            quantity: 1,
            unit_price: parseInt(formData.get("amount")),
          },
        ],
        back_urls: {
          //Rutas segun el estado de la transaccion
          success: "",
          pending: "",
          failure: "",
        },
        payer: {
          name: formData.get("first_name"),
          surname: formData.get("last_name"),
          email: formData.get("email"),
        },
        //Tunel hacia nuestro servidor local
        notification_url:
          "https://lead-dj-slide-favor.trycloudflare.com/api/payment",
      },
    });
    redirect(preference.sandbox_init_point);
  }
  return (
    <section className="bg-white my-8">
      <div className="flex flex-col sm:flex-row w-full justify-center">
        <section className="flex rounded overflow-hidden h-96 mx-auto sm:mx-0 items-end bg-gray-900 lg:col-span-5 lg:h-full xl:col-span-6">
          <Image
            src={"/donar-1.jpg"}
            width={400}
            height={400}
            alt="donar"
            className="h-full object-cover"
          />
        </section>

        <div className="flex bg-gray-50 rounded items-center justify-center px-8 py-8 sm:px-12 lg:col-span-7 lg:px-16 lg:py-2 xl:col-span-6">
          <div className="max-w-xl lg:max-w-3xl">
            <div className="">
              <Image
                src={"/mercado-pago.png"}
                width={75}
                height={75}
                alt="pass"
                className="mx-auto"
              />
            </div>
            <div className="flex gap-2 items-center justify-center">
              <h2 className="text-2xl">Donacion segura </h2>
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="size-9 text-green-400"
                >
                  <path
                    fillRule="evenodd"
                    d="M12.516 2.17a.75.75 0 0 0-1.032 0 11.209 11.209 0 0 1-7.877 3.08.75.75 0 0 0-.722.515A12.74 12.74 0 0 0 2.25 9.75c0 5.942 4.064 10.933 9.563 12.348a.749.749 0 0 0 .374 0c5.499-1.415 9.563-6.406 9.563-12.348 0-1.39-.223-2.73-.635-3.985a.75.75 0 0 0-.722-.516l-.143.001c-2.996 0-5.717-1.17-7.734-3.08Zm3.094 8.016a.75.75 0 1 0-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 0 0-1.06 1.06l2.25 2.25a.75.75 0 0 0 1.14-.094l3.75-5.25Z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
            </div>
            <form action={handleSubmit} className="mt-8 grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="FirstName"
                  className="block text-sm font-medium text-gray-700"
                >
                  First Name
                </label>

                <input
                  type="text"
                  id="FirstName"
                  name="first_name"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="LastName"
                  className="block text-sm font-medium text-gray-700"
                >
                  Last Name
                </label>

                <input
                  type="text"
                  id="LastName"
                  name="last_name"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                />
              </div>

              <div className="col-span-6">
                <label
                  htmlFor="Email"
                  className="block text-sm font-medium text-gray-700"
                >
                  {" "}
                  Email{" "}
                </label>

                <input
                  type="email"
                  id="Email"
                  name="email"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                />
              </div>

              <div className="col-span-3">
                <label
                  htmlFor="Amount"
                  className="block text-sm font-medium text-gray-700"
                >
                  {" "}
                  Amount (ARS){" "}
                </label>

                <input
                  type="number"
                  min={0}
                  id="Amount"
                  name="amount"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                />
              </div>

              <div className="col-span-6 sm:flex sm:items-center sm:gap-4">
                <button
                  type="submit"
                  className="inline-block shrink-0 rounded-md border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-transparent hover:text-blue-600 focus:outline-none focus:ring active:text-blue-500"
                >
                  Proceder con la donacion
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-center gap-4 flex-wrap my-12 ">
        {cards?.map((card, index) => (
          <div key={index} className="">
            <Image
              src={card}
              height={100}
              width={100}
              alt="card"
              className="object-cover w-16"
            />
          </div>
        ))}
      </div>
    </section>
  );
};

export default DonationPage;
