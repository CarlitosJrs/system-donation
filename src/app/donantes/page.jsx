import Donor from "@/components/Donor";
import React from "react";

async function getDonors() {
  try {
    const res = await fetch(`http://localhost:3000/api/payment`);
    if (!res.ok) {
      throw new Error("Ups! Ocurrio un error");
    }
    const donors = await res.json();
    return donors;
  } catch (error) {
    const err = error.message || "Ups! ocurrio un error";
    return err;
  }
}

const PageDonors = async () => {
  const donors = await getDonors();
  return (
    <section className="min-h-[40vh] my-8">
      <h1 className="text-3xl text-center my-8">
        Gracias a nuestros aportantes esto es posible 🐾
      </h1>
      <div className="container mx-auto grid sm:grid-cols-2 gap-4">
        {donors &&
          donors.map((donor) => <Donor key={donor._id} {...{ donor }} />)}
      </div>
    </section>
  );
};

export default PageDonors;
