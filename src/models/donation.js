const { Schema, models, model } = require("mongoose");

const donationSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  id: Number,
  amount: Number,
});

const Donation = models.Donation || model("Donation", donationSchema);

export default Donation;
