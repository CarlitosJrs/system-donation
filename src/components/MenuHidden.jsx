import Link from "next/link";
import React from "react";

const MenuHidden = ({ active, setActive }) => {
  return (
    <div
      className={`flex absolute top-full right-5 z-50 flex-col bg-blue-400 p-4 rounded-md mt-3 menu-hidden`}
    >
      <Link onClick={() => setActive(!active)} href={"/"}>
        Inicio
      </Link>
      <Link onClick={() => setActive(!active)} href={"/quienes-somos"}>
        Quienes somos?
      </Link>
      <Link onClick={() => setActive(!active)} href={"/donantes"}>
        Donantes
      </Link>
      <Link onClick={() => setActive(!active)} href={"/donar"}>
        DONÁ
      </Link>
    </div>
  );
};

export default MenuHidden;
