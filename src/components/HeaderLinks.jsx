import Link from "next/link";
import React from "react";

const HeaderLinks = () => {
  return (
    <>
      <li>
        <Link href={"/"}>Inicio</Link>
      </li>
      <li>
        <Link href={"/quienes-somos"}>Quienes somos?</Link>
      </li>
      <li>
        <Link href={"/donantes"}>Donantes</Link>
      </li>
      <li>
        <Link href={"/donar"}>DONAR</Link>
      </li>
    </>
  );
};

export default HeaderLinks;
