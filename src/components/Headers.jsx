"use client";
import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { Pacifico } from "next/font/google";
import ButtonMenu from "./ButtonMenu";
import MenuHidden from "./MenuHidden";
import HeaderLinks from "./HeaderLinks";
import Logo from "./Logo";

//Font de google
const pacifico = Pacifico({
  weight: "400",
  style: "normal",
  subsets: ["latin"],
});

const Headers = () => {
  const [active, setActive] = useState(false);

  return (
    <header
      id="header"
      className="relative flex items-center justify-around border-b p-4 "
    >
      <Logo />
      <nav>
        <ul className="hidden lg:flex gap-8  font-semibold text-slate-400 list">
          <HeaderLinks />
        </ul>
        <div className="lg:hidden">
          <button type="button" onClick={() => setActive(!active)}>
            <ButtonMenu className="h-9 w-9" />
          </button>
        </div>
      </nav>
      {active && <MenuHidden {...{ active, setActive }} />}
    </header>
  );
};

export default Headers;
