import { Pacifico } from "next/font/google";
import Image from "next/image";
import Link from "next/link";
import React from "react";

//Font de google
const pacifico = Pacifico({
  weight: "400",
  style: "normal",
  subsets: ["latin"],
});

const Logo = () => {
  return (
    <Link className="logo" href={"/"}>
      <div>
        <Image
          src={"/images-edit.png"}
          width={100}
          height={100}
          alt="logo"
          className=""
        />
      </div>
      <div
        className={`${pacifico.className} hidden md:block space-x-4 text-3xl text-blue-300`}
      >
        <span>
          <i className="fa-solid fa-paw"></i>
        </span>
        <span>Huellitas Desamparadas</span>
        <span>
          <i className="fa-solid fa-paw"></i>
        </span>
      </div>
    </Link>
  );
};

export default Logo;
