import Image from "next/image";
import Link from "next/link";
import React from "react";

const HeroSection = () => {
  return (
    <section>
      <div className="mx-auto max-w-screen-2xl px-4 py-16 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 lg:h-screen lg:grid-cols-2">
          <div className="relative z-10 lg:py-16">
            <div className="relative h-64 sm:h-80 lg:h-full">
              <Image
                src={"/perro-gato-2.jpg"}
                width={500}
                height={500}
                alt="hero"
                className="absolute inset-0 h-full w-full object-cover rounded-lg"
              />
            </div>
          </div>

          <div className="relative flex items-center bg-blue-50">
            <span className="hidden lg:absolute lg:inset-y-0 lg:-start-16 lg:block lg:w-16 lg:bg-blue-50"></span>

            <div className="p-8 sm:p-16 lg:p-24 text-gray-600">
              <h2 className="text-2xl font-bold sm:text-4xl">
                Ayuda a Nuestros Amigos de Cuatro Patas
              </h2>

              <p className="mt-4 font-semibold">
                Tu generosidad puede marcar la diferencia en la vida de perros y
                gatos callejeros. Cada donación contribuye a brindarles
                alimento, refugio y amor. Juntos, podemos ofrecerles una segunda
                oportunidad y un futuro lleno de esperanza. ¡Únete a nuestra
                causa y dona hoy mismo!
              </p>

              <Link
                href="#"
                className="mt-8 inline-block rounded border border-blue-400 bg-blue-400 px-12 py-3 text-sm font-medium text-white hover:bg-transparent hover:text-blue-400 focus:outline-none focus:ring active:text-blue-500"
              >
                ¡DONÁ AHORA!
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeroSection;
