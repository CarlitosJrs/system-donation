import Link from "next/link";
import React from "react";
import HeaderLinks from "./HeaderLinks";
import Logo from "./Logo";

const Footer = () => {
  return (
    <footer className="border-t">
      <div className="relative mx-auto max-w-screen-2xl px-4 py-16 sm:px-6 lg:px-8 lg:pt-24">
        <div className="absolute end-4 top-4 sm:end-6 sm:top-6 lg:end-8 lg:top-8">
          <Link
            className="inline-block rounded-full bg-blue-400 p-2 text-white shadow transition hover:bg-blue-300 sm:p-3 lg:p-4"
            href="#header"
          >
            <span className="sr-only">Back to top</span>

            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M14.707 12.707a1 1 0 01-1.414 0L10 9.414l-3.293 3.293a1 1 0 01-1.414-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 010 1.414z"
                clipRule="evenodd"
              />
            </svg>
          </Link>
        </div>

        <div className="lg:flex lg:items-center lg:justify-between">
          <div>
            <div className="flex justify-center text-teal-600 lg:justify-start">
              <Logo />
            </div>
          </div>

          <ul className="mt-12 flex flex-wrap justify-center gap-2 md:gap-8 lg:mt-0 lg:justify-end lg:gap-10 font-semibold text-slate-400 list">
            <HeaderLinks />
          </ul>
        </div>

        <p className="mt-12 text-center text-sm text-gray-500 lg:text-right">
          Copyright &copy; 2024. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
